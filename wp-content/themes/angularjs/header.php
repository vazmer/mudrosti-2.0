<?php
/**
 * Header template for the theme
 *
 * Displays all of the <head> section and everything up after body.
 *
 * @package Roedl Theme
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> ng-app="wordpressApp">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="initial-scale=1.0" />

    <base href="/">

    <title><?php
        // Print the <title> tag based on what is being viewed.
        global $page, $paged;

        wp_title( '|', true, 'right' );

        // Add the blog name.
        bloginfo( 'name' );

        // Add the blog description for the home/front page.
        $site_description = get_bloginfo( 'description', 'display' );
        if ( $site_description && ( is_home() || is_front_page() ) )
            echo " | $site_description";

        // Add a page number if necessary:
        if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() )
            echo ' | ' . sprintf( __( 'Page %s', 'twentyeleven' ), max( $paged, $page ) );

        ?></title>

    <?php if(get_option('mvp_favicon')) { ?><link rel="shortcut icon" href="<?php echo get_option('mvp_favicon'); ?>" /><?php } ?>

    <link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php bloginfo('rss2_url'); ?>" />
    <link rel="alternate" type="text/xml" title="RSS .92" href="<?php bloginfo('rss_url'); ?>" />
    <link rel="alternate" type="application/atom+xml" title="Atom 0.3" href="<?php bloginfo('atom_url'); ?>" />
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

    <?php if (  (function_exists('has_post_thumbnail')) && (has_post_thumbnail())  ) { ?>
        <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium-thumb' ); ?>
        <meta property="og:image" content="<?php echo $thumb['0']; ?>" />
    <?php } ?>

    <?php if ( is_single() ) { ?>
        <meta property="og:type" content="article" />
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <meta property="og:description" content="<?php echo strip_tags(get_the_excerpt()); ?>" />
            <link rel="author" href="<?php the_author_meta('googleplus'); ?>"/>
        <?php endwhile; endif; ?>
    <?php } else { ?>
        <meta property="og:description" content="<?php bloginfo('description'); ?>" />
    <?php } ?>

    <?php $favicon = atm_option('atm-favicon');?>
    <?php if($favicon):?>
        <link rel="shortcut icon" href="<?php echo $favicon['url'];?>">
    <?php endif;?>

    <?php wp_head(); ?>

    <?php echo atm_option('atm-custom-css');?>

    <?php $tracking_code = atm_option('atm-tracking-code')?>
    <?php echo !empty($tracking_code) ? '<script>'.$tracking_code.'</script>' : '';?>
</head>

<body <?php body_class(); ?>>

<header class="header" data-header></header>
