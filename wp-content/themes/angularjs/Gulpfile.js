var gulp = require('gulp');
var filter = require('gulp-filter');
var del = require('del');
var browserSync = require('browser-sync');
var sourcemaps = require('gulp-sourcemaps');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var imagemin = require('gulp-imagemin');
var spritesmith = require('gulp.spritesmith');
//var notify = require('gulp-notify');

// Assets paths
var paths = {
    sass: {
       src: 'assets/_scss/**/*.scss',
       app: 'assets/_scss/app.scss'
    },
    css: {
        src: [
            'assets/css/**/*.css',
        ],
        dest: 'assets/css/'
    },
    scripts: {
        src: [
            'assets/js/dist/**/*.js',
        ],
        dest: 'assets/js/prod/'
    },
    ngApp: {
        src: [
            'app/dist/*.js',
            'app/dist/directives/*.js',
            'app/dist/services/*.js',
            'app/dist/controllers/*.js',
            'app/dist/views/*.js',
        ],
        dest: 'app/prod/'
    },
    php: ['**/*.php'],
    html: ['views/**/*.html'],
    images: {
        sprite: {
            src: 'assets/images/dist/sprite/*.*',
            dest: 'assets/images/prod/sprite/',
            sass_des: 'assets/_scss/partials/sprites/'
        }
    }
};

// start server
gulp.task('browser-sync', function() {
    browserSync.init([paths.css.src, paths.scripts.dest+'*.js', paths.ngApp.dest+'*.js', paths.php, paths.html], {
        proxy: "http://wpng.dev",
        watchTask: true,
        debugInfo: true,
        notify: true
    });
});

// Not all tasks need to use streams
// A gulpfile is just another node program and you can use all packages available on npm
gulp.task('clean', function (cb) {
    // You can use multiple globbing patterns as you would with `gulp.src`
    del(['build'], cb);
});

// TASK: Compile Sass
gulp.task('sass', function () {
    gulp.src(paths.sass.app)
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'compressed',
            precision: 10
        }))
        .on('error', function (err) { console.log(err.message); })
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest(paths.css.dest))
        .pipe(filter(paths.css.src));
});


gulp.task('scripts', ['clean'], function () {
    // Minify and copy all JavaScript (except vendor scripts)
    // with sourcemaps all the way down
    return gulp.src(paths.scripts.src)
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(concat('scripts.js'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(paths.scripts.dest));
});

gulp.task('ng-app', ['clean'], function () {
    // Minify and copy all JavaScript
    // with sourcemaps all the way down
    return gulp.src(paths.ngApp.src)
        .pipe(sourcemaps.init())
        //.pipe(uglify())
        .pipe(concat('ng-app.js'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(paths.ngApp.dest));
});

// Generate sprite
gulp.task('sprite', function () {
    var spriteData = gulp.src(paths.images.sprite.src).pipe(spritesmith({
        imgName: 'sprite.png',
        imgPath: '../../'+paths.images.sprite.dest+'sprite.png',
        cssFormat: 'scss',
        cssName: 'sprite.scss'
    }));

    // Pipe image stream through image optimizer and onto disk
    spriteData.img
        .pipe(imagemin())
        .pipe(gulp.dest(paths.images.sprite.dest));

    //spriteData.img.pipe(gulp.dest(paths.images.sprite.dest)); // output path for the sprite
    spriteData.css.pipe(gulp.dest(paths.images.sprite.sass_des)); // output path for the SCSS
});

// Rerun the task when a file changes
gulp.task('watch', ['sass', 'scripts', 'ng-app', 'browser-sync'], function () {
    gulp.watch(paths.sass.src, ['sass']);
    gulp.watch(paths.scripts.src, ['scripts']);
    gulp.watch(paths.ngApp.src, ['ng-app']);
});

// The default task (called when you run `gulp` from cli)
gulp.task('default', ['watch', 'browser-sync']);