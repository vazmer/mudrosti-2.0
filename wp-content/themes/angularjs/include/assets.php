<?php

// enqueue styles and scripts
add_action( 'wp_enqueue_scripts', 'enqueue_theme_styles' );

// add image sizes
add_action( 'init', 'atm_image_sizes' );

// add sidebars
add_action( 'widgets_init', 'atm_init_sidebars' );


/**
 *
 */
function enqueue_theme_styles() {
    wp_enqueue_style( 'style', get_template_directory_uri().'/style.css', array(), '1.0.0');
    wp_enqueue_style( 'owl.carousel', get_template_directory_uri().'/assets/css/vendor/owl.carousel.min.css', array(), '1.0.0');
    wp_enqueue_style( 'font.open_sans', 'http://fonts.googleapis.com/css?family=Open+Sans:100italic,300italic,400italic,600italic,700italic,800italic,100,300,400,600,700,800');
    wp_enqueue_style( 'font.roboto_slab', 'http://fonts.googleapis.com/css?family=Roboto+Slab:100');
    wp_enqueue_style( 'font.dosis', 'http://fonts.googleapis.com/css?family=Dosis:200,300,400,700');
    wp_enqueue_style( 'app', get_template_directory_uri().'/assets/css/app.css', array('owl.carousel'), '1.0.0');
//    wp_enqueue_style( 'animate', 'https://raw.github.com/daneden/animate.css/master/animate.css', array('style'), '1.0.0');

    wp_register_script( 'angularjs', get_template_directory_uri() . '/assets/js/prod/vendor/angular.min.js', array(), '1.3.14', true );
    wp_register_script( 'angularjs-route', get_template_directory_uri() . '/assets/js/prod/vendor/angular-route.min.js', array(), '1.3.14', true );
    wp_register_script( 'angularjs-sanitize', get_template_directory_uri() . '/assets/js/prod/vendor/angular-sanitize.min.js', array(), '1.3.14', true );
    wp_register_script( 'modernizr', get_template_directory_uri() . '/assets/js/prod/vendor/modernizr.js', array(), '2.8.3', true );
    wp_register_script( 'fastclick', get_template_directory_uri() . '/assets/js/prod/vendor/fastclick.js', array(), '1.0.3', true );
    wp_register_script( 'placeholder', get_template_directory_uri() . '/assets/js/prod/vendor/placeholder.js', array('jquery'), '2.0.8', true );
//    wp_register_script( 'appear', get_template_directory_uri() . '/assets/js/prod/vendor/jquery.appear.js', array('jquery'), '1.0.0', true );
    wp_register_script( 'owl.carousel', get_template_directory_uri() . '/assets/js/prod/vendor/owl.carousel.min.js', array('jquery'), '1.0.0', true );
//    wp_register_script( 'lazylinepainter', get_template_directory_uri() . '/assets/js/prod/vendor/jquery.lazylinepainter-1.5.1.min.js', array('jquery'), '1.0.0', true );
    wp_register_script( 'ng-app', get_template_directory_uri() . '/app/prod/ng-app.js', array('angularjs', 'angularjs-route'), '1.0.0', true );
    wp_register_script( 'scripts', get_template_directory_uri() . '/assets/js/prod/scripts.js', array('jquery', 'owl.carousel'), '1.0.0', true );

    wp_localize_script(
        'ng-app',
        'wpLocalized',
        array(
            'views' => trailingslashit( get_template_directory_uri() ) . 'views/'
        )
    );

    wp_enqueue_script('modernizr');
    wp_enqueue_script('fastclick');
    wp_enqueue_script('placeholder');
    wp_enqueue_script('scripts');

    wp_enqueue_script('angularjs');
    wp_enqueue_script('angularjs-route');
    wp_enqueue_script('angularjs-sanitize');
    wp_enqueue_script('ng-app');
}



/**
 *
 */
function atm_init_sidebars(){

//    register_sidebar(array(
//        'id' => 'sidebar-home-bottom-widget',
//        'name' => 'Sidebar Home Bottom Widget Area',
////        'before_widget' => '<div id="%1$s" class="sidebar-widget %2$s">',
////        'after_widget' => '</div>',
////        'before_title' => '<span class="sidebar-header-wrap"><h4 class="sidebar-header">',
////        'after_title' => '</h3></span>',
//    ));
//
//    register_sidebar(array(
//        'id' => 'right-sidebar',
//        'name' => 'Right Sidebar'
//    ));

}


/**
 *
 */
function atm_image_sizes() {
    add_theme_support( 'post-thumbnails' );

//    add_image_size( 'large-thumb', 1120, 430, true );
//    add_image_size( 'list-thumb', 660, 396, true );
//    add_image_size( 'square-thumb', 400, 400, true );
//    add_image_size( 'medium-thumb', 400, 245, true );
//    add_image_size( 'medium-wide-thumb', 373, 143, true );
//    add_image_size( 'small-thumb', 120, 145, true );
//    add_image_size( 'small-wide-thumb', 225, 93, true );
}

/**
 * @param $size
 * @return bool
 */
function has_atm_post_thumbnail($size){
//    if(get_field($size))
//        return true;

    if (class_exists('MultiPostThumbnails')
        && MultiPostThumbnails::has_post_thumbnail(get_post_type(), $size)) :
        return true;

    elseif(function_exists('has_post_thumbnail') && has_post_thumbnail()) :
        return true;

    endif;

    return false;
}

/**
 * @param $size
 */
function atm_post_thumbnail($size, $attr = ''){
//    $field_image = get_field($size);
//    if($field_image){
//        echo wp_get_attachment_image($field_image, $size); return;
//    }

    if(function_exists('has_post_thumbnail') && has_post_thumbnail()) :
        return the_post_thumbnail($size, $attr);

    endif;
}

/**
 * @param $size
 * @return bool|mixed|string
 */
function atm_post_thumbnail_url($size){
    if(get_field($size))
        return the_field($size);

    if(function_exists('has_post_thumbnail') && has_post_thumbnail()) :
        $thumb = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), $size);
        return !empty($thumb[0]) ? $thumb[0] : false;

    endif;
}

