<?php

/**
 * Register Menus
 * http://codex.wordpress.org/Function_Reference/register_nav_menus#Examples
 */
register_nav_menus(array(
    'top-nav' => 'Top navigation', // registers the menu in the WordPress admin menu editor
    'footer-nav' => 'Footer navigation',
));


/**
 * Left top bar
 * http://codex.wordpress.org/Function_Reference/wp_nav_menu
 */
if ( ! function_exists( 'atm_top_nav' ) ) {
    function atm_top_nav() {
        wp_nav_menu(array(
            'container' => false,                           // remove nav container
            'container_class' => '',                        // class of container
            'menu' => '',                                   // menu name
            'menu_class' => 'top-nav',                  // adding custom nav class
            'theme_location' => 'top-nav',                  // where it's located in the theme
            'before' => '',                                 // before each link <a>
            'after' => '',                                  // after each link </a>
            'link_before' => '',                            // before each link text
            'link_after' => '',                             // after each link text
            'depth' => 1,                                   // limit the depth of the nav
            'fallback_cb' => false,                         // fallback function (see below)
            'walker' => new Walker_Nav_Menu()
        ));
    }
}

/**
 * Footer navigation
 * http://codex.wordpress.org/Function_Reference/wp_nav_menu
 */
if ( ! function_exists( 'atm_footer_nav' ) ) {
    function atm_footer_nav() {
        wp_nav_menu(array(
            'container' => false,                           // remove nav container
            'container_class' => '',                        // class of container
            'menu' => '',                                   // menu name
            'menu_class' => 'footer-nav',                   // adding custom nav class
            'theme_location' => 'footer-nav',               // where it's located in the theme
            'before' => '',                                 // before each link <a>
            'after' => '',                                  // after each link </a>
            'link_before' => '',                            // before each link text
            'link_after' => '',                             // after each link text
            'depth' => 1,                                   // limit the depth of the nav
            'fallback_cb' => false,                         // fallback function (see below)
            'walker' => new Walker_Nav_Menu()
        ));
    }
}

?>