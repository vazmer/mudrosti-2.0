<?php

if ( !function_exists( 'atm_option' ) ) {

    function atm_option($name) {
        global $am_redux;
        return isset($am_redux[$name]) ? $am_redux[$name] : false;
    }

}

if ( !function_exists( 'atm_excerpt' ) ) {

    function atm_excerpt($limit = 25, $str = false) {
        $excerpt = explode(' ', $str ? $str : get_the_excerpt(), $limit);

        if (count($excerpt)>=$limit) {
            array_pop($excerpt);
            $excerpt = implode(" ",$excerpt).'...';
        } else {
            $excerpt = implode(" ",$excerpt);
        }

        $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);

        return $excerpt;
    }

}

if ( !function_exists( 'atm_set_post_views' ) ) {

    // function to count views.
    function atm_set_post_views($postID)
    {
        $count_key = 'post_views_count';
        $count = get_post_meta($postID, $count_key, true);
        if ($count == '') {
            $count = 0;
            delete_post_meta($postID, $count_key);
            add_post_meta($postID, $count_key, '0');
        } else {
            $count++;
            update_post_meta($postID, $count_key, $count);
        }
    }

}


function atm_pagination($arrows = true, $ends = true, $pages = 3){
    if (is_singular()) return;

    global $wp_query, $paged;
    $pagination = '';

    $max_page = $wp_query->max_num_pages;
    if ($max_page == 1) return;
    if (empty($paged)) $paged = 1;

    if ($arrows && $paged > 1) $pagination .= atm_pagination_link($paged - 1, 'arrow', '&laquo;', 'Previous Page');

    if ($ends && $paged > $pages + 1) $pagination .= atm_pagination_link(1);
    if ($ends && $paged > $pages + 2) $pagination .= atm_pagination_link(1, 'unavailable', '&hellip;');

    for ($i = $paged - $pages; $i <= $paged + $pages; $i++) {
        if ($i > 0 && $i <= $max_page)
            $pagination .= atm_pagination_link($i, ($i == $paged) ? 'current' : '');
    }

    if ($ends && $paged < $max_page - $pages - 1) $pagination .= atm_pagination_link($max_page, 'unavailable', '&hellip;');
    if ($ends && $paged < $max_page - $pages) $pagination .= atm_pagination_link($max_page);

    if ($arrows && $paged < $max_page) $pagination .= atm_pagination_link($paged + 1, 'arrow', '&raquo;', 'Next Page');

    $pagination = '<ul class="pagination">' . $pagination . '</ul>';

    echo $pagination;
}

function atm_pagination_link($page, $class = '', $content = '', $title = '') {
    $id = sanitize_title_with_dashes('pagination-page-' . $page . ' ' . $class);
    $href = (strrpos($class, 'unavailable') === false && strrpos($class, 'current') === false) ? get_pagenum_link($page) : "#$id";

    $class = empty($class) ? $class : " class=\"$class\"";
    $content = !empty($content) ? $content : $page;
    $title = !empty($title) ? $title : 'Page ' . $page;

    return "<li$class><a id=\"$id\" href=\"$href\" title=\"$title\">$content</a></li>\n";
}


function aside_after_paragraph( $insertion, $paragraph_id, $content ) {
    $closing_p = '</p>';
    $paragraphs = explode( $closing_p, $content );
//    if(count($paragraphs) <= $paragraph_id + 1){
//        return $content.'<p>'.$insertion.'</p>';
//    }
    foreach ($paragraphs as $index => $paragraph) {
        if ( trim( $paragraph ) ) {
            $paragraphs[$index] .= $closing_p;
        }
        if ( $paragraph_id == $index + 1 ) {
            $paragraphs[$index] .= '<aside>'.$insertion.'</aside>';
        }
    }
    return implode( '', $paragraphs );
}

function get_query_transient($params, $expiration = 600){
    $transient_name = 'custom-query-'.json_encode($params);

    if ( false === ( $query = get_transient( $transient_name ) ) ) {
        $query = new WP_Query($params);
        set_transient( $transient_name, $query, $expiration );
    }
    return $query;
}

function get_category_color($cat){
    return get_field('category_color', $cat);
}

function get_category_color_transient($cat){
    $color_transient_name = 'color-cat-'.$cat->cat_ID;

    if ( false === ( $color = get_transient( $color_transient_name ) ) ) {
        $color = get_category_color($cat);
        set_transient( $color_transient_name, $color, 10*MINUTE_IN_SECONDS );
    }
    return $color;
}

function atm_grab_remote_image($img_src, $upload_folder, $add_to_media = true){

    $wp_upload_dir = wp_upload_dir();

    $upload_dir = $wp_upload_dir['basedir'].'/'.$upload_folder;
    $upload_dir_url = $wp_upload_dir['baseurl'].'/'.$upload_folder;

    $img_src_data = pathinfo($img_src);
    $img = $upload_dir.'/'.$img_src_data['basename'];
    $img_url = $upload_dir_url.'/'.$img_src_data['basename'];
    $img_found = true;

    if(!file_exists($img)){
        if (!file_exists($upload_dir)) {
            mkdir($upload_dir, 0777, true);
        }
        $img_found = (bool)file_put_contents($img, file_get_contents($img_src));

        if($add_to_media){
            $attach_id = atm_add_image_to_media($img);
            return $attach_id ? $attach_id : false;
        }
    }
    return $img_found ? atm_get_attachment_id_from_url($img_url) : false;
}

function atm_add_image_to_media($file_url, $parent_post_id = 0){
    // Check the type of file. We'll use this as the 'post_mime_type'.
    $filetype = wp_check_filetype( basename( $file_url ), null );

    // Prepare an array of post data for the attachment.
    $attachment = array(
        'guid'           => $file_url,
        'post_mime_type' => $filetype['type'],
        'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $file_url ) ),
        'post_content'   => '',
        'post_status'    => 'inherit'
    );

    // Insert the attachment.
    $attach_id = wp_insert_attachment( $attachment, $file_url, $parent_post_id );

    // Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
    require_once(ABSPATH . 'wp-admin/includes/image.php');

    // Generate the metadata for the attachment, and update the database record.
    $attach_data = wp_generate_attachment_metadata( $attach_id, $file_url );
    wp_update_attachment_metadata( $attach_id, $attach_data );

    return $attach_id;
}

function atm_get_attachment_id_from_url( $attachment_url = '' ) {
    global $wpdb;
    $attachment_id = false;

    // If there is no url, return.
    if ( '' == $attachment_url )
        return;

    // Get the upload directory paths
    $upload_dir_paths = wp_upload_dir();

    // Make sure the upload path base directory exists in the attachment URL, to verify that we're working with a media library image
    if ( false !== strpos( $attachment_url, $upload_dir_paths['baseurl'] ) ) {

        // If this is the URL of an auto-generated thumbnail, get the URL of the original image
        $attachment_url = preg_replace( '/-\d+x\d+(?=\.(jpg|jpeg|png|gif)$)/i', '', $attachment_url );

        // Remove the upload path base directory from the attachment URL
        $attachment_url = str_replace( $upload_dir_paths['baseurl'] . '/', '', $attachment_url );

        // Finally, run a custom database query to get the attachment ID from the modified attachment URL
        $attachment_id = $wpdb->get_var( $wpdb->prepare( "SELECT wposts.ID FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta WHERE wposts.ID = wpostmeta.post_id AND wpostmeta.meta_key = '_wp_attached_file' AND wpostmeta.meta_value = '%s' AND wposts.post_type = 'attachment'", $attachment_url ) );

    }

    return $attachment_id;
}

function atm_get_remote_url_meta_data($link){
    libxml_use_internal_errors(true);
    $html = file_get_contents($link);
    $doc = new DOMDocument();
    $doc->loadHTML($html);
    $metaData = array();
    foreach( $doc->getElementsByTagName('meta') as $meta ) {
        $metaData[$meta->getAttribute('property')] = $meta->getAttribute('content');
    }
    return $metaData;
}