(function($) {

    var imgBgDirective = wordpressApp.directive('imgBackground', imgBackgroundDirective);

    imgBgDirective.$inject = ['$timeout'];

    function imgBackgroundDirective($timeout) {
        return {
            restrict: 'A',
            link: function(scope, element, attributes) {
                $(element).css('background-image', 'url('+scope.item.source+')');
            }
        };
    }

})(jQuery);