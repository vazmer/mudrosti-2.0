(function($) {

    wordpressApp.directive('owlcarousel', owlCarouselDirective);

    function owlCarouselDirective() {

        return {
            restrict: 'A',
            link: function(scope, element, attributes) {

                var $element = $(element);
                var owlCarousel = null;
                var options = $element.data('options');

                scope.$watch(
                    function () { return $element[0].childNodes.length; },
                    function (newValue, oldValue) {
                        if (newValue !== oldValue) {

                            if (owlCarousel) {
                                owlCarousel.destroy();
                            }

                            var $height = $(window).height();
                            $element.height($height);
                            $element.find('.slider_fullscreen-slide').each(function(){
                                $(this).height($height);
                            });

                            $element.owlCarousel(options);
                            owlCarousel = $element.data('owlCarousel');
                        }
                    }
                );

            }
        };
    }

})(jQuery);