angular.module("wordpressApp").directive("footerNav", function() {
    return {
        restrict: 'A',
        templateUrl: wpLocalized.views + 'navigation/footer-nav.php',
        scope: true,
        transclude : false,
        controller: 'footerController'
    };
});