angular.module("wordpressApp").directive("headerNav", function() {
    return {
        restrict: 'A',
        templateUrl: wpLocalized.views + 'navigation/header-nav.php',
        scope: true,
        transclude : false,
        controller: 'headerController'
    };
});