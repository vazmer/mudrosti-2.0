angular.module("wordpressApp").directive("footer", function() {
    return {
        restrict: 'A',
        templateUrl: wpLocalized.views + 'footer.php',
        scope: true,
        transclude : false,
        controller: 'footerController'
    };
});