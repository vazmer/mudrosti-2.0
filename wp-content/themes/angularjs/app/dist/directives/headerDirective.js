angular.module("wordpressApp").directive("header", function() {
    return {
        restrict: 'A',
        templateUrl: wpLocalized.views + 'header.php',
        scope: true,
        transclude : false,
        controller: 'headerController'
    };
});