(function($) {

    var postController = function ($scope, $routeParams, postsService, mediaService) {
        var init = function () {
            getPost();
        };

        //get post
        var getPost = function () {
            postsService.getPost($routeParams.postSlug)
                .success(function (data) {
                    $scope.post = data[0];
                    getPostMedia($scope.post.ID);
                })
                .error(function (data, status) {
                    console.log(data + ': ' + status);
                });
        };

        //get post's media
        var getPostMedia = function (id) {
            mediaService.getPostMedia(id)
                .success(function (data) {
                    $scope.postMedia = data;
                })
                .error(function (data, status) {
                    console.log(data + ': ' + status);
                });
        };

        init();
    };

    postController.$inject = ['$scope', '$routeParams', 'postsService', 'mediaService'];

    wordpressApp.controller('postController', postController);

})(jQuery);