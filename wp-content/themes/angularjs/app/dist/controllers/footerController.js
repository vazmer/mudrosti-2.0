(function($) {

    var footerController = function ($scope, navigationService, siteService) {
        var init = function () {
            var footerMenuId = 180;

            $scope.footer = {
                copyright: '',
                menuItems: []
            };

            var setFooterNav = function(){
                navigationService.getNav(footerMenuId)
                    .success(function (data) {
                        $scope.footer.menuItems = data.items;
                    })
                    .error(function (data, status) {
                        console.log(data + ': ' + status);
                    });
            };

            var setCopyright = function(){
                siteService.getReduxOption('atm-copyright')
                    .success(function (data) {
                        $scope.footer.copyright = data.value;
                    })
                    .error(function (data, status) {
                        console.log(data + ': ' + status);
                    });
            };

            setFooterNav();
            setCopyright();
        };

        init();
    };

    footerController.$inject = ['$scope', 'navigationService', 'siteService'];

    wordpressApp.controller('footerController', footerController);

})(jQuery);