(function($) {

    var headerController = function ($scope, navigationService, siteService) {
        var init = function () {
            var headerMenuId = 180;

            $scope.header = {
                logo: '',
                menuItems: []
            };

            var setLogo = function(){
                siteService.getReduxOption('atm-logo')
                    .success(function (data) {
                        $scope.header.logo = (typeof data.value.url !== "undefined" && data.value.url) ? data.value.url : '';
                        console.log($scope.header.logo);
                    })
                    .error(function (data, status) {
                        console.log(data + ': ' + status);
                    });
            };

            var setHeaderNav = function(){
                navigationService.getNav(headerMenuId)
                    .success(function (data) {
                        $scope.header.menuItems = data.items;
                    })
                    .error(function (data, status) {
                        console.log(data + ': ' + status);
                    });
            };
            
            setLogo();
            setHeaderNav();
        };

        init();
    };

    headerController.$inject = ['$scope', 'navigationService', 'siteService'];

    wordpressApp.controller('headerController', headerController);

})(jQuery);