(function() {

    var mediaController = function ($scope, $routeParams, mediaService) {
        var init = function () {
            getSingleMedia();
        };

        //get media
        var getSingleMedia = function () {
            mediaService.getSingleMedia($routeParams.mediaSlug)
                .success(function (data) {
                    $scope.media = data[0];
                })
                .error(function (data, status) {
                    console.log(data + ': ' + status);
                });
        };

        init();
    };

    mediaController.$inject = ['$scope', '$routeParams', 'postsService'];

    wordpressApp.controller('mediaController', mediaController);

})();