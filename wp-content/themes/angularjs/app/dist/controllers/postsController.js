(function() {

    var postsController = function ($scope, postsService) {
        var init = function () {
            postsService.getPosts()
                .success(function (data) {
                    $scope.posts = data;
                })
                .error(function (data, status) {
                    console.log(data + ': ' + status);
                });
        };
        init();
    };


    postsController.$inject = ['$scope', 'postsService'];

    wordpressApp.controller('postsController', postsController);

})();