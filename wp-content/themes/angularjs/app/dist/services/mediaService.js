(function() {

    var mediaService = function($http){
        this.getMediaSingle = function(slug){
            return $http.get('wp-json/media?filter[name]='+slug, {cache:true});
        };

        this.getPostMedia = function(id){
            return $http.get('wp-json/media?filter[post_parent]='+id, {cache:true});
        };
    };

    angular.module('wordpressApp')
        .service('mediaService', mediaService);

})();