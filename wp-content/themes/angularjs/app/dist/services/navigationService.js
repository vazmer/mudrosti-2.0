(function() {

    var navigationService = function($http){
        this.getAllNavs = function(){
            return $http.get('wp-json/menus/', {cache:true});
        };
        this.getNav = function(id){
            return $http.get('wp-json/menus/'+id, {cache:true});
        };
    };

    angular.module('wordpressApp')
        .service('navigationService', navigationService);

})();