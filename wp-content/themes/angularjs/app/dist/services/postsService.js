(function(){

    var postsService = function($http){
        this.getPosts = function(){
            return $http.get('wp-json/posts', {cache:true});
        };

        this.getPost = function(slug){
            return $http.get('wp-json/posts?filter[name]='+slug, {cache:true});
        };
    };

    angular.module('wordpressApp')
        .service('postsService', postsService);

})();