(function() {

    var siteService = function($http){
        this.getReduxOption = function(name){
            return $http.get('wp-json/redux/'+name, {cache:true});
        };
    };

    angular.module('wordpressApp')
        .service('siteService', siteService);

})();