var wordpressApp = angular.module('wordpressApp', ['ngRoute'])

    .config(function($routeProvider, $locationProvider) {
        $locationProvider.html5Mode(true);

        $routeProvider
            .when('/',{
                controller:'postsController' ,
                templateUrl: wpLocalized.views + 'posts.php'
            })
            .when('/:postSlug',{
                controller:'postController' ,
                templateUrl: wpLocalized.views + 'post.php'
            })
            .when('/:postSlug/:mediaSlug',{
                controller:'mediaController' ,
                templateUrl: wpLocalized.views + 'media.php'
            })
            .otherwise({redirectTo:'/'});
    })
    .filter('toTrusted', ['$sce', function($sce) {
        return function(text) {
            return $sce.trustAsHtml(text);
        };
    }]);
angular.module("wordpressApp").directive("footer", function() {
    return {
        restrict: 'A',
        templateUrl: wpLocalized.views + 'footer.php',
        scope: true,
        transclude : false,
        controller: 'footerController'
    };
});
angular.module("wordpressApp").directive("footerNav", function() {
    return {
        restrict: 'A',
        templateUrl: wpLocalized.views + 'navigation/footer-nav.php',
        scope: true,
        transclude : false,
        controller: 'footerController'
    };
});
angular.module("wordpressApp").directive("header", function() {
    return {
        restrict: 'A',
        templateUrl: wpLocalized.views + 'header.php',
        scope: true,
        transclude : false,
        controller: 'headerController'
    };
});
angular.module("wordpressApp").directive("headerNav", function() {
    return {
        restrict: 'A',
        templateUrl: wpLocalized.views + 'navigation/header-nav.php',
        scope: true,
        transclude : false,
        controller: 'headerController'
    };
});
(function($) {

    var imgBgDirective = wordpressApp.directive('imgBackground', imgBackgroundDirective);

    imgBgDirective.$inject = ['$timeout'];

    function imgBackgroundDirective($timeout) {
        return {
            restrict: 'A',
            link: function(scope, element, attributes) {
                $(element).css('background-image', 'url('+scope.item.source+')');
            }
        };
    }

})(jQuery);
(function($) {

    wordpressApp.directive('owlcarousel', owlCarouselDirective);

    function owlCarouselDirective() {

        return {
            restrict: 'A',
            link: function(scope, element, attributes) {

                var $element = $(element);
                var owlCarousel = null;
                var options = $element.data('options');

                scope.$watch(
                    function () { return $element[0].childNodes.length; },
                    function (newValue, oldValue) {
                        if (newValue !== oldValue) {

                            if (owlCarousel) {
                                owlCarousel.destroy();
                            }

                            var $height = $(window).height();
                            $element.height($height);
                            $element.find('.slider_fullscreen-slide').each(function(){
                                $(this).height($height);
                            });

                            $element.owlCarousel(options);
                            owlCarousel = $element.data('owlCarousel');
                        }
                    }
                );

            }
        };
    }

})(jQuery);
(function() {

    var mediaService = function($http){
        this.getMediaSingle = function(slug){
            return $http.get('wp-json/media?filter[name]='+slug, {cache:true});
        };

        this.getPostMedia = function(id){
            return $http.get('wp-json/media?filter[post_parent]='+id, {cache:true});
        };
    };

    angular.module('wordpressApp')
        .service('mediaService', mediaService);

})();
(function() {

    var navigationService = function($http){
        this.getAllNavs = function(){
            return $http.get('wp-json/menus/', {cache:true});
        };
        this.getNav = function(id){
            return $http.get('wp-json/menus/'+id, {cache:true});
        };
    };

    angular.module('wordpressApp')
        .service('navigationService', navigationService);

})();
(function(){

    var postsService = function($http){
        this.getPosts = function(){
            return $http.get('wp-json/posts', {cache:true});
        };

        this.getPost = function(slug){
            return $http.get('wp-json/posts?filter[name]='+slug, {cache:true});
        };
    };

    angular.module('wordpressApp')
        .service('postsService', postsService);

})();
(function() {

    var siteService = function($http){
        this.getReduxOption = function(name){
            return $http.get('wp-json/redux/'+name, {cache:true});
        };
    };

    angular.module('wordpressApp')
        .service('siteService', siteService);

})();
(function($) {

    var footerController = function ($scope, navigationService, siteService) {
        var init = function () {
            var footerMenuId = 180;

            $scope.footer = {
                copyright: '',
                menuItems: []
            };

            var setFooterNav = function(){
                navigationService.getNav(footerMenuId)
                    .success(function (data) {
                        $scope.footer.menuItems = data.items;
                    })
                    .error(function (data, status) {
                        console.log(data + ': ' + status);
                    });
            };

            var setCopyright = function(){
                siteService.getReduxOption('atm-copyright')
                    .success(function (data) {
                        $scope.footer.copyright = data.value;
                    })
                    .error(function (data, status) {
                        console.log(data + ': ' + status);
                    });
            };

            setFooterNav();
            setCopyright();
        };

        init();
    };

    footerController.$inject = ['$scope', 'navigationService', 'siteService'];

    wordpressApp.controller('footerController', footerController);

})(jQuery);
(function($) {

    var headerController = function ($scope, navigationService, siteService) {
        var init = function () {
            var headerMenuId = 180;

            $scope.header = {
                logo: '',
                menuItems: []
            };

            var setLogo = function(){
                siteService.getReduxOption('atm-logo')
                    .success(function (data) {
                        $scope.header.logo = (typeof data.value.url !== "undefined" && data.value.url) ? data.value.url : '';
                        console.log($scope.header.logo);
                    })
                    .error(function (data, status) {
                        console.log(data + ': ' + status);
                    });
            };

            var setHeaderNav = function(){
                navigationService.getNav(headerMenuId)
                    .success(function (data) {
                        $scope.header.menuItems = data.items;
                    })
                    .error(function (data, status) {
                        console.log(data + ': ' + status);
                    });
            };
            
            setLogo();
            setHeaderNav();
        };

        init();
    };

    headerController.$inject = ['$scope', 'navigationService', 'siteService'];

    wordpressApp.controller('headerController', headerController);

})(jQuery);
(function() {

    var mediaController = function ($scope, $routeParams, mediaService) {
        var init = function () {
            getSingleMedia();
        };

        //get media
        var getSingleMedia = function () {
            mediaService.getSingleMedia($routeParams.mediaSlug)
                .success(function (data) {
                    $scope.media = data[0];
                })
                .error(function (data, status) {
                    console.log(data + ': ' + status);
                });
        };

        init();
    };

    mediaController.$inject = ['$scope', '$routeParams', 'postsService'];

    wordpressApp.controller('mediaController', mediaController);

})();
(function($) {

    var menuController = function ($scope, menuService) {
        var init = function () {

        };

        init();
    };

    menuController.$inject = ['$scope', 'menuService'];

    wordpressApp.controller('menuController', menuController);

})(jQuery);
(function($) {

    var postController = function ($scope, $routeParams, postsService, mediaService) {
        var init = function () {
            getPost();
        };

        //get post
        var getPost = function () {
            postsService.getPost($routeParams.postSlug)
                .success(function (data) {
                    $scope.post = data[0];
                    getPostMedia($scope.post.ID);
                })
                .error(function (data, status) {
                    console.log(data + ': ' + status);
                });
        };

        //get post's media
        var getPostMedia = function (id) {
            mediaService.getPostMedia(id)
                .success(function (data) {
                    $scope.postMedia = data;
                })
                .error(function (data, status) {
                    console.log(data + ': ' + status);
                });
        };

        init();
    };

    postController.$inject = ['$scope', '$routeParams', 'postsService', 'mediaService'];

    wordpressApp.controller('postController', postController);

})(jQuery);
(function() {

    var postsController = function ($scope, postsService) {
        var init = function () {
            postsService.getPosts()
                .success(function (data) {
                    $scope.posts = data;
                })
                .error(function (data, status) {
                    console.log(data + ': ' + status);
                });
        };
        init();
    };


    postsController.$inject = ['$scope', 'postsService'];

    wordpressApp.controller('postsController', postsController);

})();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC5qcyIsImZvb3RlckRpcmVjdGl2ZS5qcyIsImZvb3Rlck5hdkRpcmVjdGl2ZS5qcyIsImhlYWRlckRpcmVjdGl2ZS5qcyIsImhlYWRlck5hdkRpcmVjdGl2ZS5qcyIsImltZ0JhY2tncm91bmREaXJlY3RpdmUuanMiLCJvd2xjYXJvdXNlbERpcmVjdGl2ZS5qcyIsIm1lZGlhU2VydmljZS5qcyIsIm5hdmlnYXRpb25TZXJ2aWNlLmpzIiwicG9zdHNTZXJ2aWNlLmpzIiwic2l0ZVNlcnZpY2UuanMiLCJmb290ZXJDb250cm9sbGVyLmpzIiwiaGVhZGVyQ29udHJvbGxlci5qcyIsIm1lZGlhQ29udHJvbGxlci5qcyIsIm1lbnVDb250cm9sbGVyLmpzIiwicG9zdENvbnRyb2xsZXIuanMiLCJwb3N0c0NvbnRyb2xsZXIuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUN4QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDUkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDUkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDUkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDUkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNmQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ3ZDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ2ZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ2RBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDZkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDWEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUMxQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQzNDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDekJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ2RBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNyQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6Im5nLWFwcC5qcyIsInNvdXJjZXNDb250ZW50IjpbInZhciB3b3JkcHJlc3NBcHAgPSBhbmd1bGFyLm1vZHVsZSgnd29yZHByZXNzQXBwJywgWyduZ1JvdXRlJ10pXHJcblxyXG4gICAgLmNvbmZpZyhmdW5jdGlvbigkcm91dGVQcm92aWRlciwgJGxvY2F0aW9uUHJvdmlkZXIpIHtcclxuICAgICAgICAkbG9jYXRpb25Qcm92aWRlci5odG1sNU1vZGUodHJ1ZSk7XHJcblxyXG4gICAgICAgICRyb3V0ZVByb3ZpZGVyXHJcbiAgICAgICAgICAgIC53aGVuKCcvJyx7XHJcbiAgICAgICAgICAgICAgICBjb250cm9sbGVyOidwb3N0c0NvbnRyb2xsZXInICxcclxuICAgICAgICAgICAgICAgIHRlbXBsYXRlVXJsOiB3cExvY2FsaXplZC52aWV3cyArICdwb3N0cy5waHAnXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIC53aGVuKCcvOnBvc3RTbHVnJyx7XHJcbiAgICAgICAgICAgICAgICBjb250cm9sbGVyOidwb3N0Q29udHJvbGxlcicgLFxyXG4gICAgICAgICAgICAgICAgdGVtcGxhdGVVcmw6IHdwTG9jYWxpemVkLnZpZXdzICsgJ3Bvc3QucGhwJ1xyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAud2hlbignLzpwb3N0U2x1Zy86bWVkaWFTbHVnJyx7XHJcbiAgICAgICAgICAgICAgICBjb250cm9sbGVyOidtZWRpYUNvbnRyb2xsZXInICxcclxuICAgICAgICAgICAgICAgIHRlbXBsYXRlVXJsOiB3cExvY2FsaXplZC52aWV3cyArICdtZWRpYS5waHAnXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIC5vdGhlcndpc2Uoe3JlZGlyZWN0VG86Jy8nfSk7XHJcbiAgICB9KVxyXG4gICAgLmZpbHRlcigndG9UcnVzdGVkJywgWyckc2NlJywgZnVuY3Rpb24oJHNjZSkge1xyXG4gICAgICAgIHJldHVybiBmdW5jdGlvbih0ZXh0KSB7XHJcbiAgICAgICAgICAgIHJldHVybiAkc2NlLnRydXN0QXNIdG1sKHRleHQpO1xyXG4gICAgICAgIH07XHJcbiAgICB9XSk7IiwiYW5ndWxhci5tb2R1bGUoXCJ3b3JkcHJlc3NBcHBcIikuZGlyZWN0aXZlKFwiZm9vdGVyXCIsIGZ1bmN0aW9uKCkge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgICByZXN0cmljdDogJ0EnLFxyXG4gICAgICAgIHRlbXBsYXRlVXJsOiB3cExvY2FsaXplZC52aWV3cyArICdmb290ZXIucGhwJyxcclxuICAgICAgICBzY29wZTogdHJ1ZSxcclxuICAgICAgICB0cmFuc2NsdWRlIDogZmFsc2UsXHJcbiAgICAgICAgY29udHJvbGxlcjogJ2Zvb3RlckNvbnRyb2xsZXInXHJcbiAgICB9O1xyXG59KTsiLCJhbmd1bGFyLm1vZHVsZShcIndvcmRwcmVzc0FwcFwiKS5kaXJlY3RpdmUoXCJmb290ZXJOYXZcIiwgZnVuY3Rpb24oKSB7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICAgIHJlc3RyaWN0OiAnQScsXHJcbiAgICAgICAgdGVtcGxhdGVVcmw6IHdwTG9jYWxpemVkLnZpZXdzICsgJ25hdmlnYXRpb24vZm9vdGVyLW5hdi5waHAnLFxyXG4gICAgICAgIHNjb3BlOiB0cnVlLFxyXG4gICAgICAgIHRyYW5zY2x1ZGUgOiBmYWxzZSxcclxuICAgICAgICBjb250cm9sbGVyOiAnZm9vdGVyQ29udHJvbGxlcidcclxuICAgIH07XHJcbn0pOyIsImFuZ3VsYXIubW9kdWxlKFwid29yZHByZXNzQXBwXCIpLmRpcmVjdGl2ZShcImhlYWRlclwiLCBmdW5jdGlvbigpIHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgICAgcmVzdHJpY3Q6ICdBJyxcclxuICAgICAgICB0ZW1wbGF0ZVVybDogd3BMb2NhbGl6ZWQudmlld3MgKyAnaGVhZGVyLnBocCcsXHJcbiAgICAgICAgc2NvcGU6IHRydWUsXHJcbiAgICAgICAgdHJhbnNjbHVkZSA6IGZhbHNlLFxyXG4gICAgICAgIGNvbnRyb2xsZXI6ICdoZWFkZXJDb250cm9sbGVyJ1xyXG4gICAgfTtcclxufSk7IiwiYW5ndWxhci5tb2R1bGUoXCJ3b3JkcHJlc3NBcHBcIikuZGlyZWN0aXZlKFwiaGVhZGVyTmF2XCIsIGZ1bmN0aW9uKCkge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgICByZXN0cmljdDogJ0EnLFxyXG4gICAgICAgIHRlbXBsYXRlVXJsOiB3cExvY2FsaXplZC52aWV3cyArICduYXZpZ2F0aW9uL2hlYWRlci1uYXYucGhwJyxcclxuICAgICAgICBzY29wZTogdHJ1ZSxcclxuICAgICAgICB0cmFuc2NsdWRlIDogZmFsc2UsXHJcbiAgICAgICAgY29udHJvbGxlcjogJ2hlYWRlckNvbnRyb2xsZXInXHJcbiAgICB9O1xyXG59KTsiLCIoZnVuY3Rpb24oJCkge1xyXG5cclxuICAgIHZhciBpbWdCZ0RpcmVjdGl2ZSA9IHdvcmRwcmVzc0FwcC5kaXJlY3RpdmUoJ2ltZ0JhY2tncm91bmQnLCBpbWdCYWNrZ3JvdW5kRGlyZWN0aXZlKTtcclxuXHJcbiAgICBpbWdCZ0RpcmVjdGl2ZS4kaW5qZWN0ID0gWyckdGltZW91dCddO1xyXG5cclxuICAgIGZ1bmN0aW9uIGltZ0JhY2tncm91bmREaXJlY3RpdmUoJHRpbWVvdXQpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICByZXN0cmljdDogJ0EnLFxyXG4gICAgICAgICAgICBsaW5rOiBmdW5jdGlvbihzY29wZSwgZWxlbWVudCwgYXR0cmlidXRlcykge1xyXG4gICAgICAgICAgICAgICAgJChlbGVtZW50KS5jc3MoJ2JhY2tncm91bmQtaW1hZ2UnLCAndXJsKCcrc2NvcGUuaXRlbS5zb3VyY2UrJyknKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG59KShqUXVlcnkpOyIsIihmdW5jdGlvbigkKSB7XHJcblxyXG4gICAgd29yZHByZXNzQXBwLmRpcmVjdGl2ZSgnb3dsY2Fyb3VzZWwnLCBvd2xDYXJvdXNlbERpcmVjdGl2ZSk7XHJcblxyXG4gICAgZnVuY3Rpb24gb3dsQ2Fyb3VzZWxEaXJlY3RpdmUoKSB7XHJcblxyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIHJlc3RyaWN0OiAnQScsXHJcbiAgICAgICAgICAgIGxpbms6IGZ1bmN0aW9uKHNjb3BlLCBlbGVtZW50LCBhdHRyaWJ1dGVzKSB7XHJcblxyXG4gICAgICAgICAgICAgICAgdmFyICRlbGVtZW50ID0gJChlbGVtZW50KTtcclxuICAgICAgICAgICAgICAgIHZhciBvd2xDYXJvdXNlbCA9IG51bGw7XHJcbiAgICAgICAgICAgICAgICB2YXIgb3B0aW9ucyA9ICRlbGVtZW50LmRhdGEoJ29wdGlvbnMnKTtcclxuXHJcbiAgICAgICAgICAgICAgICBzY29wZS4kd2F0Y2goXHJcbiAgICAgICAgICAgICAgICAgICAgZnVuY3Rpb24gKCkgeyByZXR1cm4gJGVsZW1lbnRbMF0uY2hpbGROb2Rlcy5sZW5ndGg7IH0sXHJcbiAgICAgICAgICAgICAgICAgICAgZnVuY3Rpb24gKG5ld1ZhbHVlLCBvbGRWYWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAobmV3VmFsdWUgIT09IG9sZFZhbHVlKSB7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKG93bENhcm91c2VsKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb3dsQ2Fyb3VzZWwuZGVzdHJveSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciAkaGVpZ2h0ID0gJCh3aW5kb3cpLmhlaWdodCgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJGVsZW1lbnQuaGVpZ2h0KCRoZWlnaHQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJGVsZW1lbnQuZmluZCgnLnNsaWRlcl9mdWxsc2NyZWVuLXNsaWRlJykuZWFjaChmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICQodGhpcykuaGVpZ2h0KCRoZWlnaHQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJGVsZW1lbnQub3dsQ2Fyb3VzZWwob3B0aW9ucyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvd2xDYXJvdXNlbCA9ICRlbGVtZW50LmRhdGEoJ293bENhcm91c2VsJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICApO1xyXG5cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG59KShqUXVlcnkpOyIsIihmdW5jdGlvbigpIHtcclxuXHJcbiAgICB2YXIgbWVkaWFTZXJ2aWNlID0gZnVuY3Rpb24oJGh0dHApe1xyXG4gICAgICAgIHRoaXMuZ2V0TWVkaWFTaW5nbGUgPSBmdW5jdGlvbihzbHVnKXtcclxuICAgICAgICAgICAgcmV0dXJuICRodHRwLmdldCgnd3AtanNvbi9tZWRpYT9maWx0ZXJbbmFtZV09JytzbHVnLCB7Y2FjaGU6dHJ1ZX0pO1xyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIHRoaXMuZ2V0UG9zdE1lZGlhID0gZnVuY3Rpb24oaWQpe1xyXG4gICAgICAgICAgICByZXR1cm4gJGh0dHAuZ2V0KCd3cC1qc29uL21lZGlhP2ZpbHRlcltwb3N0X3BhcmVudF09JytpZCwge2NhY2hlOnRydWV9KTtcclxuICAgICAgICB9O1xyXG4gICAgfTtcclxuXHJcbiAgICBhbmd1bGFyLm1vZHVsZSgnd29yZHByZXNzQXBwJylcclxuICAgICAgICAuc2VydmljZSgnbWVkaWFTZXJ2aWNlJywgbWVkaWFTZXJ2aWNlKTtcclxuXHJcbn0pKCk7IiwiKGZ1bmN0aW9uKCkge1xyXG5cclxuICAgIHZhciBuYXZpZ2F0aW9uU2VydmljZSA9IGZ1bmN0aW9uKCRodHRwKXtcclxuICAgICAgICB0aGlzLmdldEFsbE5hdnMgPSBmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICByZXR1cm4gJGh0dHAuZ2V0KCd3cC1qc29uL21lbnVzLycsIHtjYWNoZTp0cnVlfSk7XHJcbiAgICAgICAgfTtcclxuICAgICAgICB0aGlzLmdldE5hdiA9IGZ1bmN0aW9uKGlkKXtcclxuICAgICAgICAgICAgcmV0dXJuICRodHRwLmdldCgnd3AtanNvbi9tZW51cy8nK2lkLCB7Y2FjaGU6dHJ1ZX0pO1xyXG4gICAgICAgIH07XHJcbiAgICB9O1xyXG5cclxuICAgIGFuZ3VsYXIubW9kdWxlKCd3b3JkcHJlc3NBcHAnKVxyXG4gICAgICAgIC5zZXJ2aWNlKCduYXZpZ2F0aW9uU2VydmljZScsIG5hdmlnYXRpb25TZXJ2aWNlKTtcclxuXHJcbn0pKCk7IiwiKGZ1bmN0aW9uKCl7XHJcblxyXG4gICAgdmFyIHBvc3RzU2VydmljZSA9IGZ1bmN0aW9uKCRodHRwKXtcclxuICAgICAgICB0aGlzLmdldFBvc3RzID0gZnVuY3Rpb24oKXtcclxuICAgICAgICAgICAgcmV0dXJuICRodHRwLmdldCgnd3AtanNvbi9wb3N0cycsIHtjYWNoZTp0cnVlfSk7XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgdGhpcy5nZXRQb3N0ID0gZnVuY3Rpb24oc2x1Zyl7XHJcbiAgICAgICAgICAgIHJldHVybiAkaHR0cC5nZXQoJ3dwLWpzb24vcG9zdHM/ZmlsdGVyW25hbWVdPScrc2x1Zywge2NhY2hlOnRydWV9KTtcclxuICAgICAgICB9O1xyXG4gICAgfTtcclxuXHJcbiAgICBhbmd1bGFyLm1vZHVsZSgnd29yZHByZXNzQXBwJylcclxuICAgICAgICAuc2VydmljZSgncG9zdHNTZXJ2aWNlJywgcG9zdHNTZXJ2aWNlKTtcclxuXHJcbn0pKCk7IiwiKGZ1bmN0aW9uKCkge1xyXG5cclxuICAgIHZhciBzaXRlU2VydmljZSA9IGZ1bmN0aW9uKCRodHRwKXtcclxuICAgICAgICB0aGlzLmdldFJlZHV4T3B0aW9uID0gZnVuY3Rpb24obmFtZSl7XHJcbiAgICAgICAgICAgIHJldHVybiAkaHR0cC5nZXQoJ3dwLWpzb24vcmVkdXgvJytuYW1lLCB7Y2FjaGU6dHJ1ZX0pO1xyXG4gICAgICAgIH07XHJcbiAgICB9O1xyXG5cclxuICAgIGFuZ3VsYXIubW9kdWxlKCd3b3JkcHJlc3NBcHAnKVxyXG4gICAgICAgIC5zZXJ2aWNlKCdzaXRlU2VydmljZScsIHNpdGVTZXJ2aWNlKTtcclxuXHJcbn0pKCk7IiwiKGZ1bmN0aW9uKCQpIHtcclxuXHJcbiAgICB2YXIgZm9vdGVyQ29udHJvbGxlciA9IGZ1bmN0aW9uICgkc2NvcGUsIG5hdmlnYXRpb25TZXJ2aWNlLCBzaXRlU2VydmljZSkge1xyXG4gICAgICAgIHZhciBpbml0ID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICB2YXIgZm9vdGVyTWVudUlkID0gMTgwO1xyXG5cclxuICAgICAgICAgICAgJHNjb3BlLmZvb3RlciA9IHtcclxuICAgICAgICAgICAgICAgIGNvcHlyaWdodDogJycsXHJcbiAgICAgICAgICAgICAgICBtZW51SXRlbXM6IFtdXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICB2YXIgc2V0Rm9vdGVyTmF2ID0gZnVuY3Rpb24oKXtcclxuICAgICAgICAgICAgICAgIG5hdmlnYXRpb25TZXJ2aWNlLmdldE5hdihmb290ZXJNZW51SWQpXHJcbiAgICAgICAgICAgICAgICAgICAgLnN1Y2Nlc3MoZnVuY3Rpb24gKGRhdGEpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmZvb3Rlci5tZW51SXRlbXMgPSBkYXRhLml0ZW1zO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgLmVycm9yKGZ1bmN0aW9uIChkYXRhLCBzdGF0dXMpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coZGF0YSArICc6ICcgKyBzdGF0dXMpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgdmFyIHNldENvcHlyaWdodCA9IGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgICAgICBzaXRlU2VydmljZS5nZXRSZWR1eE9wdGlvbignYXRtLWNvcHlyaWdodCcpXHJcbiAgICAgICAgICAgICAgICAgICAgLnN1Y2Nlc3MoZnVuY3Rpb24gKGRhdGEpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgJHNjb3BlLmZvb3Rlci5jb3B5cmlnaHQgPSBkYXRhLnZhbHVlO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgLmVycm9yKGZ1bmN0aW9uIChkYXRhLCBzdGF0dXMpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coZGF0YSArICc6ICcgKyBzdGF0dXMpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgc2V0Rm9vdGVyTmF2KCk7XHJcbiAgICAgICAgICAgIHNldENvcHlyaWdodCgpO1xyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIGluaXQoKTtcclxuICAgIH07XHJcblxyXG4gICAgZm9vdGVyQ29udHJvbGxlci4kaW5qZWN0ID0gWyckc2NvcGUnLCAnbmF2aWdhdGlvblNlcnZpY2UnLCAnc2l0ZVNlcnZpY2UnXTtcclxuXHJcbiAgICB3b3JkcHJlc3NBcHAuY29udHJvbGxlcignZm9vdGVyQ29udHJvbGxlcicsIGZvb3RlckNvbnRyb2xsZXIpO1xyXG5cclxufSkoalF1ZXJ5KTsiLCIoZnVuY3Rpb24oJCkge1xyXG5cclxuICAgIHZhciBoZWFkZXJDb250cm9sbGVyID0gZnVuY3Rpb24gKCRzY29wZSwgbmF2aWdhdGlvblNlcnZpY2UsIHNpdGVTZXJ2aWNlKSB7XHJcbiAgICAgICAgdmFyIGluaXQgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIHZhciBoZWFkZXJNZW51SWQgPSAxODA7XHJcblxyXG4gICAgICAgICAgICAkc2NvcGUuaGVhZGVyID0ge1xyXG4gICAgICAgICAgICAgICAgbG9nbzogJycsXHJcbiAgICAgICAgICAgICAgICBtZW51SXRlbXM6IFtdXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICB2YXIgc2V0TG9nbyA9IGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgICAgICAgICBzaXRlU2VydmljZS5nZXRSZWR1eE9wdGlvbignYXRtLWxvZ28nKVxyXG4gICAgICAgICAgICAgICAgICAgIC5zdWNjZXNzKGZ1bmN0aW9uIChkYXRhKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICRzY29wZS5oZWFkZXIubG9nbyA9ICh0eXBlb2YgZGF0YS52YWx1ZS51cmwgIT09IFwidW5kZWZpbmVkXCIgJiYgZGF0YS52YWx1ZS51cmwpID8gZGF0YS52YWx1ZS51cmwgOiAnJztcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coJHNjb3BlLmhlYWRlci5sb2dvKTtcclxuICAgICAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgICAgIC5lcnJvcihmdW5jdGlvbiAoZGF0YSwgc3RhdHVzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGEgKyAnOiAnICsgc3RhdHVzKTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgIHZhciBzZXRIZWFkZXJOYXYgPSBmdW5jdGlvbigpe1xyXG4gICAgICAgICAgICAgICAgbmF2aWdhdGlvblNlcnZpY2UuZ2V0TmF2KGhlYWRlck1lbnVJZClcclxuICAgICAgICAgICAgICAgICAgICAuc3VjY2VzcyhmdW5jdGlvbiAoZGF0YSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAkc2NvcGUuaGVhZGVyLm1lbnVJdGVtcyA9IGRhdGEuaXRlbXM7XHJcbiAgICAgICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgICAgICAuZXJyb3IoZnVuY3Rpb24gKGRhdGEsIHN0YXR1cykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhkYXRhICsgJzogJyArIHN0YXR1cyk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBzZXRMb2dvKCk7XHJcbiAgICAgICAgICAgIHNldEhlYWRlck5hdigpO1xyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIGluaXQoKTtcclxuICAgIH07XHJcblxyXG4gICAgaGVhZGVyQ29udHJvbGxlci4kaW5qZWN0ID0gWyckc2NvcGUnLCAnbmF2aWdhdGlvblNlcnZpY2UnLCAnc2l0ZVNlcnZpY2UnXTtcclxuXHJcbiAgICB3b3JkcHJlc3NBcHAuY29udHJvbGxlcignaGVhZGVyQ29udHJvbGxlcicsIGhlYWRlckNvbnRyb2xsZXIpO1xyXG5cclxufSkoalF1ZXJ5KTsiLCIoZnVuY3Rpb24oKSB7XHJcblxyXG4gICAgdmFyIG1lZGlhQ29udHJvbGxlciA9IGZ1bmN0aW9uICgkc2NvcGUsICRyb3V0ZVBhcmFtcywgbWVkaWFTZXJ2aWNlKSB7XHJcbiAgICAgICAgdmFyIGluaXQgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIGdldFNpbmdsZU1lZGlhKCk7XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgLy9nZXQgbWVkaWFcclxuICAgICAgICB2YXIgZ2V0U2luZ2xlTWVkaWEgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIG1lZGlhU2VydmljZS5nZXRTaW5nbGVNZWRpYSgkcm91dGVQYXJhbXMubWVkaWFTbHVnKVxyXG4gICAgICAgICAgICAgICAgLnN1Y2Nlc3MoZnVuY3Rpb24gKGRhdGEpIHtcclxuICAgICAgICAgICAgICAgICAgICAkc2NvcGUubWVkaWEgPSBkYXRhWzBdO1xyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgIC5lcnJvcihmdW5jdGlvbiAoZGF0YSwgc3RhdHVzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coZGF0YSArICc6ICcgKyBzdGF0dXMpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgaW5pdCgpO1xyXG4gICAgfTtcclxuXHJcbiAgICBtZWRpYUNvbnRyb2xsZXIuJGluamVjdCA9IFsnJHNjb3BlJywgJyRyb3V0ZVBhcmFtcycsICdwb3N0c1NlcnZpY2UnXTtcclxuXHJcbiAgICB3b3JkcHJlc3NBcHAuY29udHJvbGxlcignbWVkaWFDb250cm9sbGVyJywgbWVkaWFDb250cm9sbGVyKTtcclxuXHJcbn0pKCk7IiwiKGZ1bmN0aW9uKCQpIHtcclxuXHJcbiAgICB2YXIgbWVudUNvbnRyb2xsZXIgPSBmdW5jdGlvbiAoJHNjb3BlLCBtZW51U2VydmljZSkge1xyXG4gICAgICAgIHZhciBpbml0ID0gZnVuY3Rpb24gKCkge1xyXG5cclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBpbml0KCk7XHJcbiAgICB9O1xyXG5cclxuICAgIG1lbnVDb250cm9sbGVyLiRpbmplY3QgPSBbJyRzY29wZScsICdtZW51U2VydmljZSddO1xyXG5cclxuICAgIHdvcmRwcmVzc0FwcC5jb250cm9sbGVyKCdtZW51Q29udHJvbGxlcicsIG1lbnVDb250cm9sbGVyKTtcclxuXHJcbn0pKGpRdWVyeSk7IiwiKGZ1bmN0aW9uKCQpIHtcclxuXHJcbiAgICB2YXIgcG9zdENvbnRyb2xsZXIgPSBmdW5jdGlvbiAoJHNjb3BlLCAkcm91dGVQYXJhbXMsIHBvc3RzU2VydmljZSwgbWVkaWFTZXJ2aWNlKSB7XHJcbiAgICAgICAgdmFyIGluaXQgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIGdldFBvc3QoKTtcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICAvL2dldCBwb3N0XHJcbiAgICAgICAgdmFyIGdldFBvc3QgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIHBvc3RzU2VydmljZS5nZXRQb3N0KCRyb3V0ZVBhcmFtcy5wb3N0U2x1ZylcclxuICAgICAgICAgICAgICAgIC5zdWNjZXNzKGZ1bmN0aW9uIChkYXRhKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgJHNjb3BlLnBvc3QgPSBkYXRhWzBdO1xyXG4gICAgICAgICAgICAgICAgICAgIGdldFBvc3RNZWRpYSgkc2NvcGUucG9zdC5JRCk7XHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgLmVycm9yKGZ1bmN0aW9uIChkYXRhLCBzdGF0dXMpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhkYXRhICsgJzogJyArIHN0YXR1cyk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICAvL2dldCBwb3N0J3MgbWVkaWFcclxuICAgICAgICB2YXIgZ2V0UG9zdE1lZGlhID0gZnVuY3Rpb24gKGlkKSB7XHJcbiAgICAgICAgICAgIG1lZGlhU2VydmljZS5nZXRQb3N0TWVkaWEoaWQpXHJcbiAgICAgICAgICAgICAgICAuc3VjY2VzcyhmdW5jdGlvbiAoZGF0YSkge1xyXG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5wb3N0TWVkaWEgPSBkYXRhO1xyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgIC5lcnJvcihmdW5jdGlvbiAoZGF0YSwgc3RhdHVzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coZGF0YSArICc6ICcgKyBzdGF0dXMpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgaW5pdCgpO1xyXG4gICAgfTtcclxuXHJcbiAgICBwb3N0Q29udHJvbGxlci4kaW5qZWN0ID0gWyckc2NvcGUnLCAnJHJvdXRlUGFyYW1zJywgJ3Bvc3RzU2VydmljZScsICdtZWRpYVNlcnZpY2UnXTtcclxuXHJcbiAgICB3b3JkcHJlc3NBcHAuY29udHJvbGxlcigncG9zdENvbnRyb2xsZXInLCBwb3N0Q29udHJvbGxlcik7XHJcblxyXG59KShqUXVlcnkpOyIsIihmdW5jdGlvbigpIHtcclxuXHJcbiAgICB2YXIgcG9zdHNDb250cm9sbGVyID0gZnVuY3Rpb24gKCRzY29wZSwgcG9zdHNTZXJ2aWNlKSB7XHJcbiAgICAgICAgdmFyIGluaXQgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIHBvc3RzU2VydmljZS5nZXRQb3N0cygpXHJcbiAgICAgICAgICAgICAgICAuc3VjY2VzcyhmdW5jdGlvbiAoZGF0YSkge1xyXG4gICAgICAgICAgICAgICAgICAgICRzY29wZS5wb3N0cyA9IGRhdGE7XHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgLmVycm9yKGZ1bmN0aW9uIChkYXRhLCBzdGF0dXMpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhkYXRhICsgJzogJyArIHN0YXR1cyk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9O1xyXG4gICAgICAgIGluaXQoKTtcclxuICAgIH07XHJcblxyXG5cclxuICAgIHBvc3RzQ29udHJvbGxlci4kaW5qZWN0ID0gWyckc2NvcGUnLCAncG9zdHNTZXJ2aWNlJ107XHJcblxyXG4gICAgd29yZHByZXNzQXBwLmNvbnRyb2xsZXIoJ3Bvc3RzQ29udHJvbGxlcicsIHBvc3RzQ29udHJvbGxlcik7XHJcblxyXG59KSgpOyJdLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==