<?php


// ---------------------------------------------- //
// Included all required PHP assets
// ---------------------------------------------- //
require_once( ABSPATH . 'wp-admin/includes/plugin.php' ); // for supporting "is_plugin_active()" usage

require_once( get_template_directory() . '/admin/admin-init.php' );

require_once( get_template_directory() . '/include/menu/navigation.php' );
require_once( get_template_directory() . '/include/assets.php' );
require_once( get_template_directory() . '/include/helper.php' );
require_once( get_template_directory() . '/include/wp-api-query.php' );
//require_once( get_template_directory() . '/include/shortcodes/_load.php' );
//require_once( get_template_directory() . '/include/widgets/_load.php' );

//require_once( get_template_directory() . '/include/plugin/tgm/activate.php' );

//// Check if VisualComposer is activated
//if(class_exists('Vc_Manager')):
//    vc_set_shortcodes_templates_dir( get_template_directory().'/include/plugin/visual-composer/vc_templates' );
//    require_once( get_template_directory() . '/include/plugin/visual-composer/clears.php' );
//    require_once( get_template_directory() . '/include/plugin/visual-composer/helpers.php' );
//    require_once( get_template_directory() . '/include/plugin/visual-composer/param-array.php' );
//    require_once( get_template_directory() . '/include/plugin/visual-composer/mapping.php' );
//endif;

//show_admin_bar(false);

global $do_not_duplicate;
$do_not_duplicate = array();
