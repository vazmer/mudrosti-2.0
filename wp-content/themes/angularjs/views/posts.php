<ul>
    <li ng-repeat="post in posts">
        <h3>
            <a href="/{{post.slug}}" title="{{post.title}}">{{ post.title }}</a>
        </h3>
        <div ng-bind-html="post.excerpt | toTrusted"></div>
    </li>
</ul>