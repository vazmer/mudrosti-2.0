<article class="post">

    <div class="slider-wrapper slider_fullscreen-wrapper">

        <?php
        $options = array(
            'items' => 1,
            'loop' => false,
            'nav' => true,
            'dots' => false,
            'animateIn' => false,
            'animateOut' => false,
            'smartSpeed' => 0,
            'navText' => ['<span></span>', '<span></span>'],
        );
        ?>

        <section class="slider slider_fullscreen" owlcarousel data-options="<?php echo htmlentities(json_encode($options));?>">

            <div ng-repeat="item in postMedia" class="slider_fullscreen-slide slide">

                <h2 class="slider_fullscreen-slide-caption" ng-bind-html="item.excerpt | toTrusted"></h2>

                <div class="slider_fullscreen-slide-bg" img-background></div>

            </div>

        </section>

    </div>


</article>