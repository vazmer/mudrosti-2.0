<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'angularjs_wp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '621(;x~qn<W5=T,b+Q$u5,Y}4|-_($57+4w-RMJ1_uPjzV`b:`DVTd>3SXpG6aKr');
define('SECURE_AUTH_KEY',  't1`g*T[PPp=F1/sQLxa+_D_J?TGEhy`(IcfvGz}h5`0:f;ngY[mYAmAAfAxXF;j`');
define('LOGGED_IN_KEY',    'T~`ce/tGCyZf/HYA&rBz813ECqIj[wcM-+N0AEq,KFEaGPyod2W?MWK7.Wp~?JaO');
define('NONCE_KEY',        'Qljl KmdXp(Gl:>Ux^9CB/;p;1/w[67B.i2k&GII[+2(]TL)fUa*d7]tF08lrJf~');
define('AUTH_SALT',        ',.l0CQ3~.i7;P}^Tz5%Hx0$dQ{C[yKH+8:D0RLjLeLP-Z |D~Hbv_B4ngMP@>}>2');
define('SECURE_AUTH_SALT', 'Gm>/gHyep=D+^@`A8B.BPcvE!xn]mFQDbzXVM5OU$0Rty%YXs%*<nj]xMz%ABE*o');
define('LOGGED_IN_SALT',   'PTb7CodhT1?p;qgmI|(M|rn3A97RU(5}jncFd%PvI;_z<z@#/w!M}*0H kKk/9{Z');
define('NONCE_SALT',       'gP{W8M}`@_<mQWrkQh9sxSc^iKd*FL 4qk<l&<+!$<i oh&p9~f---CN7k6)$7Ik');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
