# AngularJS Wordpress theme

This is a WP Theme that uses AngularJS framework!

## Requirements

You'll need to have the following items installed before continuing.

  * [Node.js](http://nodejs.org): Use the installer provided on the NodeJS website.
  * [Gulp](http://gulpjs.com/): Run `[sudo] npm install -g gulp-cli`
  * [Bower](http://bower.io): Run `[sudo] npm install -g bower`

## Quickstart

```bash
git clone git@bitbucket.org:vazmer/mudrosti-2.0.git
cd wp-content/themes/angularjs
npm install && bower install
```


![](http://i.imgur.com/IrPw9UP.jpg?raw=true)


Navigate to `wp-content/themes/angularjs/app` to edit the AngularJS files.

While you're working on your project, run:

`gulp`

And you're set!